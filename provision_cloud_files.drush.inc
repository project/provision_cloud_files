<?php

/**
 * @file
 * Provision/drush hooks for providing context and provisioning drupal config.
 */

define('HOSTING_CLOUD_FILES_RACKSPACE_AUTH_URL', 'https://identity.api.rackspacecloud.com/v2.0/');

/**
 * Implements hook_provision_drupal_config().
 */
function provision_cloud_files_provision_drupal_config($uri, $data) {
  $settings = '';

  if ($uri && d()->type === 'site'){
    $status = drush_get_option('cloud_files_status');
    $scheme = drush_get_option('cloud_files_scheme');
    $region = drush_get_option('cloud_files_region');
    $username = drush_get_option('cloud_files_username');
    $api_key = drush_get_option('cloud_files_api_key');
    $cdn = drush_get_option('cloud_files_cdn_domain');

    // If successful, set settings.php variables.
    if ($status && !empty($cdn)){
      $settings .= '$' . "conf['file_default_scheme'] = '$scheme';\n";
      $settings .= '$' . "conf['rackspace_cloud_auth_url'] = '" . HOSTING_CLOUD_FILES_RACKSPACE_AUTH_URL . "';\n";
      $settings .= '$' . "conf['rackspace_cloud_region'] = '$region';\n";
      $settings .= '$' . "conf['rackspace_cloud_username'] = '$username';\n";
      $settings .= '$' . "conf['rackspace_cloud_api_key'] = '$api_key';\n";
      $settings .= '$' . "conf['rackspace_cloud_container'] = '$uri';\n";
      $settings .= '$' . "conf['rackspace_cloud_cdn_domain'] = '$cdn';\n";
      $settings .= '$' . "conf['rackspace_cloud_cdn_ssl_domain'] = '$cdn';\n";
      $settings .= "\n";
    }
  }

  return $settings;
}
